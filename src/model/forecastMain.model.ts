export interface ForecastMain{
    temp: number;
    temp_min: number;
    temp_max: number;
    pressure: string;
    sea_level:string;
    humidity: string;
}