import { City } from "./city.model";
import { Forecast } from "./forecast.model";

export interface MainReport{
    city: City;
    list: Forecast[];
}