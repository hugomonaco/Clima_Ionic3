import { Coord } from "./coord.model";



export interface City{
    name: string;
    country: string;
    coord : Coord;
}

