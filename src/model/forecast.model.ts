import { Clouds } from "./clouds.model";
import {Weather} from './weather.model';
import {ForecastMain} from './forecastMain.model';
import {Wind} from './wind.model';

export interface Forecast{
    dt_txt: string;
    main: ForecastMain; 
    weather: Weather[];
    clouds: Clouds;
    wind: Wind;
}