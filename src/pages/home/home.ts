import { Component } from '@angular/core';
import { IonicPage, NavController, Events, LoadingController, Loading } from 'ionic-angular';
import {ForecastServiceProvider} from '../../providers/forecast-service/forecast-service';
import { DbServiceProvider } from '../../providers/db-service/db-service';
import {MainReport} from '../../model/MainReport.model';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  cities: any= null;
 
  constructor(private db: DbServiceProvider,  private geolocation: Geolocation,private forecastProvider: ForecastServiceProvider,
    private navCtrl: NavController, private event:Events, private loading: LoadingController) {
      event.subscribe('city:delete',() =>{ this.ionViewDidEnter()});
  }

  
  ionViewDidLoad(){
    
    let loader:Loading = this.loading.create({content: "Loading Cities..."});
    loader.present();
    this.db.openDb()
      .then(() =>{
        this.db.createTableCities().then(()=>{
          this.db.getCities().then((res)=> {
            loader.dismiss();
            if(res.rows.length === 0){
              this.findByGeo(true);
            }else{
              this.loadCities(res);
            }
           });
        }
        );           
      });
  }

async ionViewDidEnter(){
    const res = await this.db.getCities();
    this.loadCities(res); 
  }

  loadCities(res){
    this.cities = [];
    for(var i = 0; i < res.rows.length; i++){
      this.cities.push({
        id: res.rows.item(i).id,
        name: res.rows.item(i).name,
        country: res.rows.item(i).country,
      });
    }
  }


  //save indica si la ciudad debe guardarse en favoritos
  findByGeo(save:boolean){
    let loader:Loading = this.loading.create({content: "Searching your location..."});
    loader.present();
    this.geolocation.getCurrentPosition().then((resp) => {
      loader.dismiss();
      this.forecastProvider.getForecastByGeo(resp.coords.latitude,resp.coords.longitude)
      .subscribe((data: MainReport) => this.showReport(data, save) );
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  showReport(report: MainReport, save:boolean){
    this.navCtrl.push('ForecastPage',{report:report, save:save});  
    }
  
}
