import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Forecast } from '../../model/forecast.model';

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  forecast: Forecast; 
  iconUrl: string ;
  constructor(private navParams: NavParams) {}

  ionViewWillLoad() {
    this.forecast = this.navParams.get('forecast');
    this.iconUrl= `http://openweathermap.org/img/w/${this.forecast.weather[0].icon}.png` ;
  }

  kelvinToCelsius(Kelvin){
    return Math.round(Kelvin - 273.15);
   }
  
   //--> m/s to km/h
  transformWindSpeed(speed){
    return Math.round(speed * 36)/10; 
  }

}
