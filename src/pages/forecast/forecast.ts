import { Component } from '@angular/core';
import { IonicPage, NavParams, ToastController } from 'ionic-angular';
import {MainReport} from '../../model/MainReport.model';
import { DbServiceProvider } from '../../providers/db-service/db-service';

@IonicPage()
@Component({
  selector: 'page-forecast',
  templateUrl: 'forecast.html',
})
export class ForecastPage {


  report: MainReport = null;
  favorite: boolean= false ;

  constructor(private toast: ToastController, private db:DbServiceProvider,
    private navParams: NavParams) {
  }

  ionViewDidEnter() {
       this.report = this.navParams.get('report');
       this.isFavorite();
       if(this.navParams.get('save') && !this.favorite){
          this.saveCity();
       }
  }

  saveCity(){
    let city={
      cityName: this.report.city.name,
      country: this.report.city.country
    };
    
    this.db.saveCity(city).then((res)=>{
      this.toast.create({
        message: `${this.report.city.name} has been added to your favorites.`,
        duration: 3000,
      }).present();
      this.isFavorite();
    },(err)=>{
      this.toast.create({
        message: 'Error: ' + err,
        duration: 3000,
      }).present();
      });
  }

  isFavorite(){
    let city={
      cityName: this.report.city.name,
      country: this.report.city.country
    }
    this.db.getCityByName(city).then((res)=> {
      if(res.rows.length === 0){
        this.favorite = false;
      }else{
       this.favorite = true;
      }
    } );
  }

}
