import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { MainReport } from '../../model/MainReport.model';
import {ForecastServiceProvider} from '../../providers/forecast-service/forecast-service';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  city: string;
  country: string;

  constructor( private navCtrl: NavController, private forecastService: ForecastServiceProvider) {
  }

  showForecast(){

    this.forecastService.getForecastByName(this.city, this.extractCountryCode())
    .subscribe((data: MainReport) => this.navCtrl.push('ForecastPage',{report:data}));
  }

  extractCountryCode():string{
    if(this.country.length > 2){
      return this.country.substr(0,2);
    }
    return this.country;
  }

}
