import { Component, Input } from '@angular/core';
import {NavController } from 'ionic-angular';
import {Forecast} from '../../model/forecast.model';


@Component({
  selector: 'forecast',
  templateUrl: 'forecast.html'
})
export class ForecastComponent {
  
  @Input() forecast: Forecast;
    
  constructor(private navCtrl: NavController) {
  
  }

  showDetail(){
    this.navCtrl.push("DetailPage",{
      forecast:this.forecast
    });
  }

  kelvinToCelsius(Kelvin){
   return Math.round(Kelvin - 273.15 );
  }


}
