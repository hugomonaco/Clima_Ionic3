import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular';
import { CityComponent } from './city/city';
import { ForecastComponent } from './forecast/forecast';
@NgModule({
	declarations: [CityComponent,
    ForecastComponent,
		
	],
	imports: [
		IonicModule
	],
	exports: [CityComponent,
    ForecastComponent,
		
	]
})
export class ComponentsModule {}
