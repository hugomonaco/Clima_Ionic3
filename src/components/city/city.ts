import { Component, Input } from '@angular/core';
import {NavController, ToastController, Events } from 'ionic-angular';
import {ForecastServiceProvider} from '../../providers/forecast-service/forecast-service';
import { MainReport } from '../../model/MainReport.model';
import { DbServiceProvider } from '../../providers/db-service/db-service';


@Component({
  selector: 'city',
  templateUrl: 'city.html'
})
export class CityComponent {

  constructor( private navCtrl: NavController, private forecastService: ForecastServiceProvider,
    private toast: ToastController, private db: DbServiceProvider, private event:Events) {
  }

  @Input() city: any;

 
  showForecast(){
    this.forecastService.getForecastByName(this.city.name, this.city.country)
    .subscribe((data: MainReport) => this.navCtrl.push('ForecastPage',{report:data}) );
  }


  deleteCity(){
    this.db.deleteCity(this.city.id).then((res)=>{
      this.toast.create({
        message: `${this.city.name} has been removed from favorites`,
        duration: 3000,
      }).present();
      this.event.publish('city:delete');
      },(err)=>{ 
        this.toast.create({
          message: 'Error: '+err,
          duration: 3000,
        }).present();
       });
  }
}

