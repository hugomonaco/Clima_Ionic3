import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DbServiceProvider {

  db : SQLiteObject = null;

  constructor(private sqlite: SQLite){
    
  }

  public openDb(){
    return this.sqlite.create({
        name: 'cities.db',
        location: 'default'
    })
    .then((db: SQLiteObject) => {
     this.db =db;
   })
}

public createTableCities(){
  return this.db.executeSql("create table if not exists cities( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, country TEXT)",{})
}

public saveCity(city){
  let sql = "INSERT INTO cities (name, country) values (?,?)";
  return this.db.executeSql(sql,[city.cityName, city.country]);
}


public getCities(){
  let sql = "SELECT * FROM cities";
  return this.db.executeSql(sql,{});
}


public getCityByName(city:any){
  let sql = "SELECT * FROM cities WHERE name = ? and country = ?";
  return this.db.executeSql(sql,[city.cityName, city.country]);
}

deleteCity(id){
  let sql = "DELETE FROM cities WHERE cities.id = ? ";
  return this.db.executeSql(sql,[id]);
}

}
