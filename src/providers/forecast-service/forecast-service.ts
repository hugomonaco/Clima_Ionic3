import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { MainReport } from '../../model/MainReport.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class ForecastServiceProvider {

  url: string="http://api.openweathermap.org/data/2.5/forecast?";
  apiKey: string ="&APPID=7c796f7595bad988a1d1dee45cad68e7";

  constructor(private http: Http) {
    
  }

  //api.openweathermap.org/data/2.5/forecast?q={city name},{country code}&APPID={APIKEY}
  getForecastByName(city: string, country: string): Observable<MainReport>{
    var endPoint: string = this.url  + 'q=' + city + ',' + country + this.apiKey;
    return this.http.get(endPoint)
    .map(this.extractData)
    .catch(this.handleError)
  }

  //api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&APPID={APIKEY}
  getForecastByGeo(lat:number, long:number): Observable<MainReport>{
    var endPoint: string = this.url  + 'lat=' + lat + '&lon=' + long + this.apiKey;
    return this.http.get(endPoint)
    .map(this.extractData)
    .catch(this.handleError)
  }

  private handleError(error: Response){
    return Observable.throw(error.json() || "Server error");
  }

  private extractData(response: Response){
    console.log(response.json());
    return response.json();
  }


}
