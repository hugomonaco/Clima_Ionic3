webpackJsonp([1],{

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(283);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HomePageModule = (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__city_city__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__ = __webpack_require__(285);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__city_city__["a" /* CityComponent */],
                __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__["a" /* ForecastComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_2__city_city__["a" /* CityComponent */],
                __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__["a" /* ForecastComponent */],
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CityComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CityComponent = (function () {
    function CityComponent(navCtrl, forecastService, toast, db, event) {
        this.navCtrl = navCtrl;
        this.forecastService = forecastService;
        this.toast = toast;
        this.db = db;
        this.event = event;
    }
    CityComponent.prototype.showForecast = function () {
        var _this = this;
        this.forecastService.getForecastByName(this.city.name, this.city.country)
            .subscribe(function (data) { return _this.navCtrl.push('ForecastPage', { report: data }); });
    };
    CityComponent.prototype.deleteCity = function () {
        var _this = this;
        this.db.deleteCity(this.city.id).then(function (res) {
            _this.toast.create({
                message: _this.city.name + " has been removed from favorites",
                duration: 3000,
            }).present();
            _this.event.publish('city:delete');
        }, function (err) {
            _this.toast.create({
                message: 'Error: ' + err,
                duration: 3000,
            }).present();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CityComponent.prototype, "city", void 0);
    CityComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'city',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/city/city.html"*/'<ion-card  >\n  <ion-card-header (click)="showForecast()" >\n      <ion-card-title>{{city.name}}</ion-card-title>\n  </ion-card-header>\n\n  <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>{{city.country}}</ion-col>\n          <ion-col col-3>\n              <button ion-button color="secondary"  (click)="deleteCity()" >\n                <ion-icon name="trash" >\n              </ion-icon></button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n  </ion-card-content>\n\n</ion-card>'/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/city/city.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__["a" /* ForecastServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__["a" /* DbServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], CityComponent);
    return CityComponent;
}());

//# sourceMappingURL=city.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForecastComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ForecastComponent = (function () {
    function ForecastComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ForecastComponent.prototype.showDetail = function () {
        this.navCtrl.push("DetailPage", {
            forecast: this.forecast
        });
    };
    ForecastComponent.prototype.kelvinToCelsius = function (Kelvin) {
        return Math.round(Kelvin - 273.15);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ForecastComponent.prototype, "forecast", void 0);
    ForecastComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'forecast',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/forecast/forecast.html"*/'<ion-card margin  (click)="showDetail()">\n    <ion-card-header  >\n        <ion-card-title color="secondary" >{{forecast.dt_txt | date: \'dd/MM/yyyy HH:mm\'}}</ion-card-title>\n    </ion-card-header>\n     \n    <ion-card-content>\n        <label>{{forecast.weather[0].main}} ({{forecast.weather[0].description}})</label><br>\n        <label>Max: {{ kelvinToCelsius(forecast.main.temp_max)}} °C</label><br>\n        <label>Min: {{ kelvinToCelsius(forecast.main.temp_min)}} °C</label><br>       \n    </ion-card-content>  \n</ion-card> '/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/forecast/forecast.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], ForecastComponent);
    return ForecastComponent;
}());

//# sourceMappingURL=forecast.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var HomePage = (function () {
    function HomePage(db, geolocation, forecastProvider, navCtrl, event, loading) {
        var _this = this;
        this.db = db;
        this.geolocation = geolocation;
        this.forecastProvider = forecastProvider;
        this.navCtrl = navCtrl;
        this.event = event;
        this.loading = loading;
        this.cities = null;
        event.subscribe('city:delete', function () { _this.ionViewDidEnter(); });
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loader = this.loading.create({ content: "Loading Cities..." });
        loader.present();
        this.db.openDb()
            .then(function () {
            _this.db.createTableCities().then(function () {
                _this.db.getCities().then(function (res) {
                    loader.dismiss();
                    if (res.rows.length === 0) {
                        _this.findByGeo(true);
                    }
                    else {
                        _this.loadCities(res);
                    }
                });
            });
        });
    };
    HomePage.prototype.ionViewDidEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.db.getCities()];
                    case 1:
                        res = _a.sent();
                        this.loadCities(res);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.loadCities = function (res) {
        this.cities = [];
        for (var i = 0; i < res.rows.length; i++) {
            this.cities.push({
                id: res.rows.item(i).id,
                name: res.rows.item(i).name,
                country: res.rows.item(i).country,
            });
        }
    };
    //save indica si la ciudad debe guardarse en favoritos
    HomePage.prototype.findByGeo = function (save) {
        var _this = this;
        var loader = this.loading.create({ content: "Searching your location..." });
        loader.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            loader.dismiss();
            _this.forecastProvider.getForecastByGeo(resp.coords.latitude, resp.coords.longitude)
                .subscribe(function (data) { return _this.showReport(data, save); });
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
    };
    HomePage.prototype.showReport = function (report, save) {
        this.navCtrl.push('ForecastPage', { report: report, save: save });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>My cities</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding >\n  <div *ngIf=\'cities\'>\n    <city *ngFor="let city of cities" [city]=city ></city>\n  </div>\n    <ion-fab bottom right >\n      <button ion-fab color="secondary" >...</button>\n      <ion-fab-list side="top">\n        <button ion-fab color="secondary" navPush="SearchPage">\n          <ion-icon name="search"></ion-icon>\n        </button>\n      </ion-fab-list>\n      <ion-fab-list side="left">\n          <button ion-fab color="secondary" (click)="findByGeo(false)">Geo</button>\n      </ion-fab-list>\n    </ion-fab>\n\n   \n   \n</ion-content>\n'/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__["a" /* DbServiceProvider */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__["a" /* ForecastServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=1.js.map