webpackJsonp([3],{

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailPageModule", function() { return DetailPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail__ = __webpack_require__(286);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetailPageModule = (function () {
    function DetailPageModule() {
    }
    DetailPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detail__["a" /* DetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detail__["a" /* DetailPage */]),
            ],
        })
    ], DetailPageModule);
    return DetailPageModule;
}());

//# sourceMappingURL=detail.module.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetailPage = (function () {
    function DetailPage(navParams) {
        this.navParams = navParams;
    }
    DetailPage.prototype.ionViewWillLoad = function () {
        this.forecast = this.navParams.get('forecast');
        this.iconUrl = "http://openweathermap.org/img/w/" + this.forecast.weather[0].icon + ".png";
    };
    DetailPage.prototype.kelvinToCelsius = function (Kelvin) {
        return Math.round(Kelvin - 273.15);
    };
    //--> m/s to km/h
    DetailPage.prototype.transformWindSpeed = function (speed) {
        return Math.round(speed * 36) / 10;
    };
    DetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detail',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/detail/detail.html"*/'<ion-header>\n  <ion-navbar *ngIf=\'forecast\' color="primary">\n    <ion-title>{{forecast.dt_txt | date: \'dd/MM/yyyy HH:mm\'}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding *ngIf=\'forecast\'> \n    <ion-grid>\n        <ion-row border>\n            <ion-col col-4 *ngIf="iconUrl"><img [src]="iconUrl" width="80%" height="80%"> </ion-col>\n            <ion-col col-4 *ngIf="iconUrl"><img [src]="iconUrl" width="80%" height="80%"> </ion-col>\n            <ion-col col-4 *ngIf="iconUrl"><img [src]="iconUrl" width="80%" height="80%"> </ion-col>\n          </ion-row>\n        <ion-row border>\n          <ion-col col-6>Weather:</ion-col>\n          <ion-col col-6>{{forecast.weather[0].main}} ({{forecast.weather[0].description}})</ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>Temperature:</ion-col>\n          <ion-col col-6>{{ kelvinToCelsius(forecast.main.temp)}} °C</ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-6>Temperature Max:</ion-col>\n          <ion-col col-6>{{ kelvinToCelsius(forecast.main.temp_max)}} °C</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Temperature Min:</ion-col>\n          <ion-col col-6>{{ kelvinToCelsius(forecast.main.temp_min)}} °C</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Clouds(%):</ion-col>\n          <ion-col col-6>{{forecast.clouds.all}} %</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Wind speed:</ion-col>\n          <ion-col col-6>{{transformWindSpeed(forecast.wind.speed)}} Km/h</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Humidity (%):</ion-col>\n          <ion-col col-6>{{forecast.main.humidity}} %</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Atmospheric pressure:</ion-col>\n          <ion-col col-6>{{forecast.main.pressure}} hPa</ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>Atmospheric pressure on the sea level:</ion-col>\n          <ion-col col-6>{{forecast.main.sea_level}} hPa</ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/detail/detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], DetailPage);
    return DetailPage;
}());

//# sourceMappingURL=detail.js.map

/***/ })

});
//# sourceMappingURL=3.js.map