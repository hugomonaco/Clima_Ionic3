webpackJsonp([0],{

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(283);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SearchPageModule = (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search__["a" /* SearchPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__city_city__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__ = __webpack_require__(285);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__city_city__["a" /* CityComponent */],
                __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__["a" /* ForecastComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_2__city_city__["a" /* CityComponent */],
                __WEBPACK_IMPORTED_MODULE_3__forecast_forecast__["a" /* ForecastComponent */],
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CityComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CityComponent = (function () {
    function CityComponent(navCtrl, forecastService, toast, db, event) {
        this.navCtrl = navCtrl;
        this.forecastService = forecastService;
        this.toast = toast;
        this.db = db;
        this.event = event;
    }
    CityComponent.prototype.showForecast = function () {
        var _this = this;
        this.forecastService.getForecastByName(this.city.name, this.city.country)
            .subscribe(function (data) { return _this.navCtrl.push('ForecastPage', { report: data }); });
    };
    CityComponent.prototype.deleteCity = function () {
        var _this = this;
        this.db.deleteCity(this.city.id).then(function (res) {
            _this.toast.create({
                message: _this.city.name + " has been removed from favorites",
                duration: 3000,
            }).present();
            _this.event.publish('city:delete');
        }, function (err) {
            _this.toast.create({
                message: 'Error: ' + err,
                duration: 3000,
            }).present();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CityComponent.prototype, "city", void 0);
    CityComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'city',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/city/city.html"*/'<ion-card  >\n  <ion-card-header (click)="showForecast()" >\n      <ion-card-title>{{city.name}}</ion-card-title>\n  </ion-card-header>\n\n  <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>{{city.country}}</ion-col>\n          <ion-col col-3>\n              <button ion-button color="secondary"  (click)="deleteCity()" >\n                <ion-icon name="trash" >\n              </ion-icon></button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n  </ion-card-content>\n\n</ion-card>'/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/city/city.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__["a" /* ForecastServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_db_service_db_service__["a" /* DbServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], CityComponent);
    return CityComponent;
}());

//# sourceMappingURL=city.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForecastComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ForecastComponent = (function () {
    function ForecastComponent(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ForecastComponent.prototype.showDetail = function () {
        this.navCtrl.push("DetailPage", {
            forecast: this.forecast
        });
    };
    ForecastComponent.prototype.kelvinToCelsius = function (Kelvin) {
        return Math.round(Kelvin - 273.15);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ForecastComponent.prototype, "forecast", void 0);
    ForecastComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'forecast',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/forecast/forecast.html"*/'<ion-card margin  (click)="showDetail()">\n    <ion-card-header  >\n        <ion-card-title color="secondary" >{{forecast.dt_txt | date: \'dd/MM/yyyy HH:mm\'}}</ion-card-title>\n    </ion-card-header>\n     \n    <ion-card-content>\n        <label>{{forecast.weather[0].main}} ({{forecast.weather[0].description}})</label><br>\n        <label>Max: {{ kelvinToCelsius(forecast.main.temp_max)}} °C</label><br>\n        <label>Min: {{ kelvinToCelsius(forecast.main.temp_min)}} °C</label><br>       \n    </ion-card-content>  \n</ion-card> '/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/components/forecast/forecast.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], ForecastComponent);
    return ForecastComponent;
}());

//# sourceMappingURL=forecast.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchPage = (function () {
    function SearchPage(navCtrl, forecastService) {
        this.navCtrl = navCtrl;
        this.forecastService = forecastService;
    }
    SearchPage.prototype.showForecast = function () {
        var _this = this;
        this.forecastService.getForecastByName(this.city, this.extractCountryCode())
            .subscribe(function (data) { return _this.navCtrl.push('ForecastPage', { report: data }); });
    };
    SearchPage.prototype.extractCountryCode = function () {
        if (this.country.length > 2) {
            return this.country.substr(0, 2);
        }
        return this.country;
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/search/search.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Search</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item>\n    <ion-label floating >City</ion-label>\n    <ion-input type="tex" required [(ngModel)]="city"> </ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating >Country</ion-label>\n    <ion-input type="tex" required [(ngModel)]="country"> </ion-input>\n  </ion-item>\n  <button ion-button round margin color="secondary" (click)="showForecast()">Search </button>\n</ion-content>\n'/*ion-inline-end:"/home/hugo/proyectos/simplex/Ionic_clima/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_forecast_service_forecast_service__["a" /* ForecastServiceProvider */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ })

});
//# sourceMappingURL=0.js.map